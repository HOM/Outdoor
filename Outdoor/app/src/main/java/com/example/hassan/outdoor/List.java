package com.example.hassan.outdoor;
import java.util.ArrayList;

public class List {
    private String name ;
    private ArrayList<Place> places ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Place> getPlaces() {
        return places;
    }

    public void setPlaces(ArrayList<Place> places) {
        this.places = places;
    }

    public void addPlace(Place newPlace)
    {

    }

    public void removePlace(Place removedPlace)
    {

    }

    public void sortBy(ArrayList<Integer> sortingMethod)
    {

    }
}
